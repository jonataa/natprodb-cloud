@extends('app')

@section('content')
<div class="container">
	@if (Session::has('message'))
		<div class="alert alert-success">{{ Session::get('message') }}</div>
	@endif
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<td>Name</td>
				<td>TestName</td>
				<td style="width:20em;">Actions</td>
			</tr>
		</thead>
		<tbody>
		@foreach($molecules as $molecule)
			<tr>
				<td>
					<a href="{{ url('/molecules', $molecule->id) }}">
						{{ $molecule->name }}
					</a>
				</td>
				<td>{{ $molecule->testname }}</td>
				<td>
					<a href="{{ url('/molecules', $molecule->id) }}" class="btn btn-success">
						<i class="glyphicon glyphicon-eye-open"></i> See
					</a>
					<a href="{{ url('/molecules/edit', $molecule->id) }}" class="btn btn-primary">
						<i class="glyphicon glyphicon-pencil"></i> Edit
					</a>
					<a href="{{ url('/molecules/edit', $molecule->id) }}" class="btn btn-danger">
						<i class="glyphicon glyphicon-trash"></i> Delete
					</a>
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>
@endsection
