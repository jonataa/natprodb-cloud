@extends('app')

@section('content')
<div class="container">

	<h1>Create a new Molecule</h1>

	<hr/>

	@if ($errors->any())
		<ul class="alert alert-danger" style="list-style-type: none;">
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	@endif

	{!! Form::open(['url' => 'molecules']) !!}
		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('testname', 'TestName:') !!}
			{!! Form::text('testname', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Store', ['class' => 'btn btn-primary form-control']) !!}
		</div>
	{!! Form::close() !!}
</div>
@endsection
