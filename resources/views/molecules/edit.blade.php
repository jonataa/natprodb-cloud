@extends('app')

@section('content')
<div class="container">

	<h1>Edit the Molecule</h1>

	<hr/>
{!! Form::model($molecule, ['url' => ['molecules/update', $molecule->id], 'method' => 'PUT']) !!}
		<div class="form-group">
			{!! Form::label('name', 'Name:') !!}
			{!! Form::text('name', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('testname', 'TestName:') !!}
			{!! Form::text('testname', null, ['class' => 'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Store', ['class' => 'btn btn-primary form-control']) !!}
		</div>
	{!! Form::close() !!}
</div>
@endsection
