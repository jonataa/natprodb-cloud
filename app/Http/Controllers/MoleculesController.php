<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMoleculeRequest;
use App\Http\Controllers\Controller;
use App\Molecule;

class MoleculesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$molecules = Molecule::latest()->get();

		return view('molecules.index', compact('molecules'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('molecules.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreateMoleculeRequest  $request)
	{
		Molecule::create($request->all());

		return redirect('molecules');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$molecule = Molecule::findOrFail($id);

		return view('molecules.show', compact('molecule'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$molecule = Molecule::find($id);

		return view('molecules.edit', compact('molecule'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(CreateMoleculeRequest  $request, $id)
	{
		$molecule = Molecule::findOrFail($id);
		$update = $request->all();

		$molecule->name = $update['name'];
		$molecule->testname = $update['testname'];

		$molecule->save();

		return redirect('molecules')->with('message', 'Hueeeeeeeeeeeee!!!!');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
