<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::get('molecules', 'MoleculesController@index');
Route::get('molecules/create', 'MoleculesController@create');
Route::get('molecules/edit/{id}', 'MoleculesController@edit');
Route::put('molecules/update/{id}', 'MoleculesController@update');
Route::get('molecules/{id}', 'MoleculesController@show');
Route::post('molecules', 'MoleculesController@store');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
