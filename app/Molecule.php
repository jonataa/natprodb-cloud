<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Molecule extends Model {

		protected $table = 'molecules';

		protected $fillable = ['name', 'testname'];
}
