<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create([
			'name'		=> 'Admin',
			'email'		=> 'diego_leite@live.com',
			'password'	=> bcrypt('1234567'),
			'isAdmin'	=> true
		]);
	}
}
