<?php

use Illuminate\Database\Seeder;
use App\Molecule;

class MoleculeTableSeeder extends Seeder {

	public function run()
	{
		DB::table('molecules')->delete();

		Molecule::create([
			'name'		=>	'Zoeira',
			'testname'	=>	'huehue'
		]);
	}
}
